"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lintModuleIdentifiersNewline = exports.ruleMeta = void 0;
exports.ruleMeta = {
    type: 'suggestion',
    fixable: 'whitespace',
    schema: [
        {
            enum: ['always', 'never'],
            default: 'always',
        },
    ],
    docs: {
        description: 'Enforce placing import or export identifier on a newline',
        recommended: 'warn',
    },
    messages: {
        missingNewline: 'Import or Export identifier should start on a new line'
    }
};
function lintModuleIdentifiersNewline({ identifiers, context, node }) {
    if (identifiers.length < 2) {
        return;
    }
    const report = (fixer) => context.report({
        node,
        messageId: 'missingNewline',
        fix: fixer,
    });
    const sourceCode = context.getSourceCode();
    for (let i = 1; i < identifiers.length; i++) {
        const curr = identifiers[i];
        const prev = identifiers[i - 1];
        const firstTokenOfCurrentProperty = sourceCode.getFirstToken(curr);
        const propertiesOnSameLine = curr.loc.start.line === prev.loc.start.line;
        if (firstTokenOfCurrentProperty === null) {
            continue;
        }
        if (propertiesOnSameLine) {
            const comma = sourceCode.getTokenBefore(firstTokenOfCurrentProperty);
            if (comma === null) {
                continue;
            }
            const rangeAfterComma = [comma.range[1], firstTokenOfCurrentProperty.range[0]];
            if (sourceCode.text.slice(rangeAfterComma[0], rangeAfterComma[1]).trim()) {
                continue;
            }
            report((fixer) => fixer.replaceTextRange(rangeAfterComma, '\n'));
        }
    }
}
exports.lintModuleIdentifiersNewline = lintModuleIdentifiersNewline;
