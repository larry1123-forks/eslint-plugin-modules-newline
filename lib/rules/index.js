"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const import_declaration_newline_1 = (0, tslib_1.__importDefault)(require("./import-declaration-newline"));
const export_declaration_newline_1 = (0, tslib_1.__importDefault)(require("./export-declaration-newline"));
exports.default = {
    'import-declaration-newline': import_declaration_newline_1.default,
    'export-declaration-newline': export_declaration_newline_1.default,
};
