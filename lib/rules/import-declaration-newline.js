"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../util");
const declaration_newline_utils_1 = require("./declaration-newline-utils");
exports.default = (0, util_1.createRule)({
    name: 'import-declaration-newline',
    meta: declaration_newline_utils_1.ruleMeta,
    defaultOptions: ['always'],
    create: (context) => {
        return {
            ImportDeclaration(node) {
                (0, declaration_newline_utils_1.lintModuleIdentifiersNewline)({
                    node,
                    context,
                    identifiers: node.specifiers
                });
            },
        };
    }
});
