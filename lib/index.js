"use strict";
const tslib_1 = require("tslib");
const rules_1 = (0, tslib_1.__importDefault)(require("./rules"));
const plugin = {
    rules: rules_1.default,
    configs: {
        recommended: {
            rules: {
                '@larry1123/modules-newline/export-declaration-newline': 'error',
                '@larry1123/modules-newline/import-declaration-newline': 'error',
            },
        },
    },
};
module.exports = plugin;
