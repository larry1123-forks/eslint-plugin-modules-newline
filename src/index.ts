import rules from './rules'

const plugin = {
  rules: rules,
  configs: {
    recommended: {
      rules: {
        '@larry1123/modules-newline/export-declaration-newline': 'error',
        '@larry1123/modules-newline/import-declaration-newline': 'error',
      },
    },
  },
}

export = plugin
