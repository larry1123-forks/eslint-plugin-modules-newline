import type {
  TSESLint,
} from '@typescript-eslint/experimental-utils'

import {
  ESLintUtils,
} from '@typescript-eslint/experimental-utils'

export const createRule = ESLintUtils.RuleCreator(
  _name => {
    // TODO set a real docs url
    return `https://gitlab.com/larry1123-forks/eslint-plugin-modules-newline/-/blob/main/README.md`;
  },
)

export type CreateRuleMetaDocs = Omit<TSESLint.RuleMetaDataDocs, 'url'>;
export type CreateRuleMeta<TMessageIds extends string> = {
  docs: CreateRuleMetaDocs;
} & Omit<TSESLint.RuleMetaData<TMessageIds>, 'docs'>
