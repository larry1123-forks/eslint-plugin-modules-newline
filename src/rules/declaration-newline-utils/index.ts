import type {
  TSESTree,
} from '@typescript-eslint/types'
import type {
  TSESLint,
} from '@typescript-eslint/experimental-utils'

import type {
  CreateRuleMeta,
} from '../../util'

export type ExportNamedDeclaration = TSESTree.ExportNamedDeclaration
export type ExportDefaultDeclaration = TSESTree.ExportDefaultDeclaration
export type ImportDeclaration = TSESTree.ImportDeclaration

export type MessageIds =
  | 'missingNewline'

export type Options = [
    'never' | 'always',
]

export const ruleMeta: CreateRuleMeta<MessageIds> = {
  type: 'suggestion',
  fixable: 'whitespace',
  schema: [
    {
      enum: ['always', 'never'],
      default: 'always',
    },
  ],
  docs: {
    description: 'Enforce placing import or export identifier on a newline',
    recommended: 'warn',
  },
  messages: {
    missingNewline: 'Import or Export identifier should start on a new line'
  }
}

type lintModuleIdentifiersNewlineOptions = {
  identifiers: Array<TSESTree.Node>
  context: TSESLint.RuleContext<MessageIds, Options>
  node: TSESTree.Node
}

export function lintModuleIdentifiersNewline({identifiers, context, node}: lintModuleIdentifiersNewlineOptions): void {
  if (identifiers.length < 2) {
    return
  }

  const report = (fixer: TSESLint.ReportFixFunction) => context.report({
    node,
    messageId: 'missingNewline',
    fix: fixer,
  })
  const sourceCode = context.getSourceCode()

  for (let i = 1; i < identifiers.length; i++) {
    const curr = identifiers[i]
    const prev = identifiers[i - 1]
    const firstTokenOfCurrentProperty = sourceCode.getFirstToken(curr)
    const propertiesOnSameLine = curr.loc.start.line === prev.loc.start.line

    if (firstTokenOfCurrentProperty === null) {
      continue
    }

    if (propertiesOnSameLine) {
      const comma = sourceCode.getTokenBefore(firstTokenOfCurrentProperty)
      if (comma === null) {
        continue
      }
      const rangeAfterComma: TSESLint.AST.Range = [comma.range[1], firstTokenOfCurrentProperty.range[0]]
      if (sourceCode.text.slice(rangeAfterComma[0], rangeAfterComma[1]).trim()) {
        continue
      }
      report((fixer) => fixer.replaceTextRange(rangeAfterComma, '\n'));
    }
  }
}
