import type {
  ImportDeclaration,
  Options,
  MessageIds,
} from './declaration-newline-utils'

import {
  createRule,
} from '../util'
import {
  ruleMeta,
  lintModuleIdentifiersNewline,
} from './declaration-newline-utils'

export default createRule<Options, MessageIds>({
  name: 'import-declaration-newline',
  meta: ruleMeta,
  defaultOptions: ['always'],
  create: (context) => {
    return {
      ImportDeclaration(node: ImportDeclaration): void {
        lintModuleIdentifiersNewline({
          node,
          context,
          identifiers: node.specifiers
        })
      },
    }
  }
})
