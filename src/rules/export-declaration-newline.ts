import type {
  ExportNamedDeclaration,
  ExportDefaultDeclaration,
  Options,
  MessageIds,
} from './declaration-newline-utils'

import {
  createRule,
} from '../util'
import {
  ruleMeta,
  lintModuleIdentifiersNewline,
} from './declaration-newline-utils'

export default createRule<Options, MessageIds>({
  name: 'export-declaration-newline',
  meta: ruleMeta,
  defaultOptions: ['always'],
  create: (context) => {
    return {
      ExportDefaultDeclaration(node: ExportDefaultDeclaration): void {
        if (node.declaration.type === 'ObjectExpression') {
          lintModuleIdentifiersNewline({
            node,
            context,
            identifiers: node.declaration.properties
          })
        }
      },
      ExportNamedDeclaration(node: ExportNamedDeclaration): void {
        lintModuleIdentifiersNewline({
          node,
          context,
          identifiers: node.specifiers
        })
      }
    }
  }
})
