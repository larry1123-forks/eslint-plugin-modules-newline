import importDeclarationNewline from './import-declaration-newline'
import exportDeclarationNewline from './export-declaration-newline'

export default {
  'import-declaration-newline': importDeclarationNewline,
  'export-declaration-newline': exportDeclarationNewline,
}
